#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Program proxy
"""

import socket
import sys
import time
import socketserver
import json
import os.path as path
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


def write_log(sent, stri, ip, port, error):
    sent = sent.replace("\r\n", " ")
    tiempo = time.time()
    if error != "true":
        # write message in the log file
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        time_s = h + m + s
        current_tm = time.strftime('%Y-%m-%d', time.localtime(tiempo))
        time_mess_log = current_tm + " " + str(time_s) + " "
        sent = time_mess_log + stri + ip + ":" + port + ": " + sent + "\r\n"
    else:
        # write message error in the log file"""
        current_tm = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
        sent = current_tm + " " + stri + ip + " port " + port + "\r\n"

    with open(log, 'a') as log_file:
        log_file.write(sent)


class XML_Handler(ContentHandler):

    # Init the variables of xml file
    def __init__(self):
        self.list = []
        self.log = ""
        self.inlog = False
        self.data_file = {}
        self.elementDicc = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwdpath"],
        }

    # Start Element
    def startElement(self, name, att):
        if name != "config":
            if name == 'log':
                self.inlog = True
            else:
                for atr in self.elementDicc[name]:
                    self.data_file[name + "_" + atr] = att.get(atr, "")

    def characters(self, char):
        if self.inlog:
            self.log += char

    def get_log(self):
        return self.log

    # return a dictionary of labels
    def get_labels(self):
        return self.data_file


# ECHO SERVER CLASS
class SIP_Handler(socketserver.DatagramRequestHandler):

    # variables
    user_dicc = {}

    # check if registered.json exists
    def json2register(self):
        try:
            if path.exists(users_file):
                with open(users_file, "r") as json_file:
                    self.user_dicc = json.load(json_file)
        except Exception:
            pass

    # check if a client has expired and delete it
    def registrar(self):
        usser_dicc_2 = []

        time_now = int(time.time())
        for user in self.user_dicc:
            if self.user_dicc[user][3] < time_now:
                usser_dicc_2.append(user)
        for user in usser_dicc_2:
            del self.user_dicc[user]

    # write the collection of clients to the file
    def register2json(self):
        print("se escribe en el fichero")
        with open(users_file, "w") as json_file:
            json.dump(self.user_dicc, json_file, indent=1)
            print(self.user_dicc)

    # The server
    def handle(self):
        line = self.rfile.read()
        line_decode = line.decode('utf-8')
        print("The client sends us: " + line_decode)
        write_log(line_decode, "Received from ", self.client_address[0],
                  str(self.client_address[1]), "false")
        line_split = line_decode.split(" ")
        method = line_split[0]
        method_list = ["REGISTER", "INVITE", "BYE", "ACK"]
        client_address = line_split[1].split(":")[1]

        if method not in method_list:
            answer = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
            self.wfile.write(bytes(answer, 'utf-8'))
            write_log(answer, "Send to ", self.client_address[0],
                      str(self.client_address[1]), "false")
        elif not line_split[1].startswith("sip:"):
            answer = "SIP/2.0 400 Bad Request\r\n\r\n"
            self.wfile.write(bytes(answer, 'utf-8'))
            write_log(answer, "Send to ", self.client_address[0],
                      str(self.client_address[1]), "false")
        elif not line_split[2].startswith("SIP/2.0"):
            answer = "SIP/2.0 400 Bad Request\r\n\r\n"
            self.wfile.write(bytes(answer, 'utf-8'))
            write_log(answer, "Send to ", self.client_address[0],
                      str(self.client_address[1]), "false")
        else:
            if method == "REGISTER":
                # we collect the data that interest us for the client
                client_ip = self.client_address[0]
                register_time = int(time.time())
                expires_time = register_time + int(line_split[3])
                client_port = line_split[1].split(":")[2]
                client_data = [client_ip, client_port, register_time,
                               expires_time]

                # we save the data or delete it according to the expire
                self.json2register()
                self.user_dicc[client_address] = client_data
                self.registrar()
                if int(line_split[3]) == 0:
                    del self.user_dicc[client_address]
                self.register2json
                print(self.user_dicc)

                # answer the REGISTER message
                answer = "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(answer, 'utf-8'))
                print("Send ----- " + answer)
                write_log(answer, "Send to ", client_ip,
                          str(client_port), "false")
            elif method == "INVITE" or method == "ACK" or method == "BYE":
                self.json2register()
                self.registrar()
                print(self.user_dicc)
                if client_address not in self.user_dicc:
                    answer = "SIP/2.0 404 User Not Found\r\n\r\n"
                    self.wfile.write(bytes(answer, 'utf-8'))
                    print("Send ----- " + answer)
                    write_log(answer, "Send to ", self.client_address[0],
                              str(self.client_address[1]), "false")
                else:
                    invite_ip = self.user_dicc[client_address][0]
                    invite_port = int(self.user_dicc[client_address][1])

                    try:
                        with socket.socket(socket.AF_INET,
                                           socket.SOCK_DGRAM) as my_socket:
                            my_socket.setsockopt(socket.SOL_SOCKET,
                                                 socket.SO_REUSEADDR, 1)
                            my_socket.connect((invite_ip, invite_port))

                            # send the message to the other ua
                            my_socket.send(bytes(line_decode, 'utf-8'))
                            print("Send ----- " + line_decode)
                            write_log(line_decode, "Send to ", invite_ip,
                                      str(invite_port), "false")

                            # recive messages and send to the other ua
                            buffer_data = my_socket.recv(1024)
                            buffer_message = buffer_data.decode('utf-8')
                            print("The client sends us: " + buffer_message)
                            write_log(buffer_message, "Received from ",
                                      invite_ip, str(invite_port), "false")

                            self.wfile.write(buffer_data)
                            print("Send ----- " + buffer_message)
                            write_log(buffer_message, "Send to ",
                                      self.client_address[0],
                                      str(self.client_address[1]), "false")
                    except ConnectionRefusedError:
                        error_message = "Error: No server listening at "
                        write_log(message, error_message, proxy_server,
                                  str(proxy_port), "true")

if __name__ == "__main__":

    # read arguments
    try:
        if len(sys.argv) > 2 or len(sys.argv) < 2:
            sys.exit("Usage: python proxy_registrar.py config")
        else:
            config = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python proxy_registrar.py config")

    # Parser
    parser = make_parser()
    file_Handler = XML_Handler()
    parser.setContentHandler(file_Handler)
    parser.parse(open(config))
    data_file = file_Handler.get_labels()

    # variables config
    my_name = data_file["server_name"]
    my_port = data_file["server_puerto"]
    my_ip = data_file["server_ip"]
    if my_ip == "":
        my_ip = '127.0.0.1'
    users_file = data_file["database_path"]
    log = file_Handler.get_log()

    # created the server
    print("Server " + my_name + " listening at port " + my_port +
          "...\r\n\r\n")
    serv = socketserver.UDPServer((my_ip, int(my_port)), SIP_Handler)

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("server end")
