#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Program uaclient
"""

import socket
import sys
import time
import simplertp
import socketserver
import secrets
import random
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from uaclient import XML_Handler


def write_log(sent, stri, ip, port, error):
    sent = sent.replace("\r\n", " ")
    tiempo = time.time()
    if error != "true":
        # write message in the log file
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        time_s = h + m + s
        current_tm = time.strftime('%Y-%m-%d', time.localtime(tiempo))
        time_mess_log = current_tm + " " + str(time_s) + " "
        sent = time_mess_log + stri + ip + ":" + port + ": " + sent + "\r\n"
    else:
        # write message error in the log file"""
        current_tm = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
        sent = current_tm + " " + stri + ip + " port " + port + "\r\n"

    with open(log, 'a') as log_file:
        log_file.write(sent)


# ECHO SERVER CLASScho server class
class SIP_Handler(socketserver.DatagramRequestHandler):

    # variables
    dicc = {}

    # send the message RTP, send the audio
    def send_audio(self):
        bit = secrets.randbelow(1)
        aleat = random.randint(0, 17)

        csrc = []
        i = 0
        while i < aleat:
            random_num = random.randint(0, 99999)
            csrc.append(random_num)
            i += 1

        RTP_header = simplertp.RtpHeader()
        RTP_header.set_header(version=2, marker=bit, payload_type=14,
                              cc=aleat)
        RTP_header.setCSRC(csrc)
        audio = simplertp.RtpPayloadMp3(audio_file)
        simplertp.send_rtp_packet(RTP_header, audio, self.dicc["invite_ip"],
                                  self.dicc["invite_port_rtp"])

    # The server
    def handle(self):
        # received a message
        line = self.rfile.read()
        line_decode = line.decode('utf-8')
        print("The client sends us: " + line_decode)
        write_log(line_decode, "Received from ", self.client_address[0],
                  str(self.client_address[1]), "false")
        line_split = line_decode.split(" ")
        line_list = line_decode.split('\r\n')
        method = line_split[0]
        method_list = ["INVITE", "BYE", "ACK"]

        # posibles anwers to de message
        if method not in method_list:
            answer = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
            self.wfile.write(bytes(answer, 'utf-8'))
            write_log(answer, "Sent to ", self.client_address[0],
                      str(self.client_address[1]), "false")
            print("Send ----- " + answer)
        elif not line_split[1].startswith("sip:"):
            answer = "SIP/2.0 400 Bad Request\r\n\r\n"
            write_log(answer, "Sent to ", self.client_address[0],
                      str(self.client_address[1]), "false")
            print("Send ----- " + answer)
        elif not line_split[2].startswith("SIP/2.0"):
            answer = "SIP/2.0 400 Bad Request\r\n\r\n"
            swrite_log(answer, "Sent to ", self.client_address[0],
                       str(self.client_address[1]), "false")
            print("Send ----- " + answer)
        else:
            if method == "INVITE":
                # we collect the data that interest us for the invite message
                invite_ip = line_list[5].split(" ")[1]
                invite_port_rtp = line_list[8].split(" ")[1]

                self.dicc["invite_ip"] = invite_ip
                self.dicc["invite_port_rtp"] = int(invite_port_rtp)

                # answer invite message
                answer_1 = "SIP/2.0 100 Trying\r\n\r\n"
                answer_2 = "SIP/2.0 180 Ringing\r\n\r\n"
                answer_3 = "SIP/2.0 200 OK\r\n\r\n"

                content_type = "Content-Type: application/sdp\r\n"

                # sdp
                v = "v=0\r\n"
                o = "o=" + my_sip_address + " " + my_ip + "\r\n"
                s = "s=misesion\r\n"
                t = "t=0\r\n"
                m = "m=audio " + my_rtp_port + " RTP" + "\r\n"
                sdp = v + o + s + t + m

                length_sdp = str(len(bytes(sdp, 'utf-8')))
                content_length = "Content-Length: " + length_sdp + "\r\n\r\n"

                sdp_message = content_type + content_length + sdp

                final_answer = answer_1 + answer_2 + answer_3 + sdp_message
                self.wfile.write(bytes(final_answer, 'utf-8'))
                write_log(final_answer, "Sent to ", self.client_address[0],
                          str(self.client_address[1]), "false")
                print("Send -----  " + final_answer)
            elif method == "ACK":
                print("Sending audio...")
                self.send_audio()
                print("Audio send finished.")
                write_log(audio_file, "Sending audio to ",
                          self.dicc['invite_ip'],
                          str(self.dicc['invite_port_rtp']), "false")
            elif method == "BYE":
                answer = "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(answer, 'utf-8'))
                write_log(answer, "Sent to ", self.client_address[0],
                          str(self.client_address[1]), "false")
                print("Send -----  " + answer)

if __name__ == '__main__':
    # read arguments
    try:
        if len(sys.argv) > 2 or len(sys.argv) < 2:
            sys.exit("Usage: python uaserver.py config")
        else:
            config = sys.argv[1]

    except IndexError:
        sys.exit("Usage: python uaserver.py config")

    # Parser
    parser = make_parser()
    file_Handler = XML_Handler()
    parser.setContentHandler(file_Handler)
    parser.parse(open(config))
    data_file = file_Handler.get_labels()

    # variables config
    my_sip_address_port = data_file["account_username"]
    proxy_server = data_file["regproxy_ip"]
    proxy_port = int(data_file["regproxy_puerto"])
    log = data_file["log_path"]
    my_sip_address = my_sip_address_port.split(":")[0]
    my_rtp_port = data_file["rtpaudio_puerto"]
    my_ip = data_file["uaserver_ip"]
    my_port = data_file["uaserver_puerto"]
    audio_file = data_file["audio_path"]

    # created the server
    print("Listening...")
    serv = socketserver.UDPServer((my_ip, int(my_port)), SIP_Handler)

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("server end")
