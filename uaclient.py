#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Program uaclient
"""

import socket
import sys
import time
import secrets
import random
import simplertp
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class XML_Handler(ContentHandler):

    # Init the variables of xml file
    def __init__(self):
        self.data_file = {}
        self.elementDicc = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    # Start Element.
    def startElement(self, name, att):
        if name != "config":
            for atr in self.elementDicc[name]:
                self.data_file[name + "_" + atr] = att.get(atr, "")

    # return a dictionary of labels
    def get_labels(self):
        return self.data_file


def send_audio(audio_file, invite_ip, invite_port_rtp):
    bit = secrets.randbelow(1)
    aleat = random.randint(0, 17)

    csrc = []
    i = 0
    while i < aleat:
        random_num = random.randint(0, 99999)
        csrc.append(random_num)
        i += 1

    RTP_header = simplertp.RtpHeader()
    RTP_header.set_header(version=2, marker=bit, payload_type=14,
                          cc=aleat)
    RTP_header.setCSRC(csrc)
    audio = simplertp.RtpPayloadMp3(audio_file)
    simplertp.send_rtp_packet(RTP_header, audio, invite_ip,
                              invite_port_rtp)


def write_log(sent, stri, ip, port, error):
    sent = sent.replace("\r\n", " ")
    tiempo = time.time()
    if error != "true":
        # write message in the log file
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        time_s = h + m + s
        current_tm = time.strftime('%Y-%m-%d', time.localtime(tiempo))
        time_mess_log = current_tm + " " + str(time_s) + " "
        sent = time_mess_log + stri + ip + ":" + port + ": " + sent + "\r\n"
    else:
        # write message error in the log file"""
        current_tm = time.strftime('%Y%m%d%H%M%S', time.localtime(tiempo))
        sent = current_tm + " " + stri + ip + " port " + port + "\r\n"

    with open(log, 'a') as log_file:
        log_file.write(sent)


if __name__ == '__main__':
    # read arguments
    try:
        if len(sys.argv) > 4 or len(sys.argv) < 4:
            sys.exit("Usage: python uaclient.py config method option")
        else:
            config = sys.argv[1]
            method = sys.argv[2]
            opcion = sys.argv[3]

    except IndexError:
        sys.exit("Usage: python uaclient.py config method option")

    # Parser
    parser = make_parser()
    file_Handler = XML_Handler()
    parser.setContentHandler(file_Handler)
    try:
        parser.parse(open(config))
        data_file = file_Handler.get_labels()
    except FileNotFoundError:
        sys.exit("Usage: python uaclient.py config method option")

    # variables config
    my_sip_address_port = data_file["account_username"]
    proxy_server = data_file["regproxy_ip"]
    proxy_port = int(data_file["regproxy_puerto"])
    log = data_file["log_path"]
    my_sip_address = my_sip_address_port.split(":")[0]
    my_rtp_port = data_file["rtpaudio_puerto"]
    audio_file = data_file["audio_path"]

    # Messages SIP
    if method == "REGISTER":
        sip_message = method + " sip:" + my_sip_address_port + " SIP/2.0\r\n"

        # Expire
        try:
            int(opcion)
        except ValueError:
            sys.exit("Usage: python uaclient.py config method option")
        expires_message = "Expires: " + str(opcion) + ("\r\n\r\n")

        message = sip_message + expires_message

    elif method == "INVITE":
        invite_message = method + " sip:" + opcion + " SIP/2.0\r\n"

        content_type = "Content-Type: application/sdp\r\n"

        # sdp
        v = "v=0\r\n"
        o = "o=" + my_sip_address + " " + proxy_server + "\r\n"
        s = "s=misesion\r\n"
        t = "t=0\r\n"
        m = "m=audio " + my_rtp_port + " RTP" + "\r\n"
        sdp = v + o + s + t + m

        length_sdp = str(len(bytes(sdp, 'utf-8')))
        content_length = "Content-Length: " + length_sdp + "\r\n\r\n"

        sdp_message = content_type + content_length + sdp

        message = invite_message + sdp_message

    elif method == "BYE":
        message = method + " sip:" + opcion + " SIP/2.0\r\n\r\n"

    # Socket
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxy_server, proxy_port))
        my_socket.send(bytes(message, 'utf-8'))
        print("Send: " + message)
        write_log(message, "Sent to ", proxy_server, str(proxy_port), "false")

        # the message we read from the proxy
        try:
            buffer_data = my_socket.recv(1024)
            buffer_message = buffer_data.decode('utf-8')
        except ConnectionRefusedError:
            error_message = "Error: No server listening at "
            write_log(message, error_message, proxy_server, str(proxy_port),
                      "true")

        if method == "INVITE":
            try:
                if buffer_message.startswith('SIP/2.0 100 Trying\r\n\r\n' +
                                             'SIP/2.0 180 Ringing\r\n\r\n' +
                                             'SIP/2.0 200 OK\r\n'):
                    # Recive invite
                    print('Received-- ', buffer_message)
                    write_log(buffer_message, "Received from ", proxy_server,
                              str(proxy_port), "false")

                    # Send ack
                    message = "ACK" + " sip:" + opcion + " SIP/2.0\r\n\r\n"
                    print("Send: " + message)
                    my_socket.send(bytes(message, 'utf-8') + b'\r\n')
                    write_log(message, "Sent to ", proxy_server,
                              str(proxy_port), "false")
                    # Send audio
                    user_invite = buffer_message.split('\r\n')[10]
                    invite_ip = user_invite.split(' ')[1]
                    audio_invite = buffer_message.split('\r\n')[13]
                    invite_port_rtp = int(audio_invite.split(' ')[1])
                    print("Sending audio...")
                    send_audio(audio_file, invite_ip, invite_port_rtp)
                    print("Audio send finished.")
                    write_log(audio_file, "Sending audio to ", invite_ip,
                              str(invite_port_rtp), "false")
                else:
                    print('Received-- ', buffer_message)
                    write_log(buffer_message, "Received from ", proxy_server,
                              str(proxy_port), "false")
            except NameError:
                print("message INVITE error")
        else:
            try:
                print('Received-- ', buffer_message)
                write_log(buffer_message, "Received from ", proxy_server,
                          str(proxy_port), "false")
            except NameError:
                if method == "BYE":
                    print("message BYE error")
                elif method == "REGISTER":
                    print("message REGISTER error")

        print("close socket...")
    print("End.")
